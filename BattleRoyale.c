#include <stdio.h>
#include <stdlib.h>

typedef struct Enemy {
  int x, y;
} Enemy;
Enemy* Init(unsigned size) {
  Enemy *enemies = (Enemy *)calloc(size, sizeof(Enemy) );
  return enemies;
}

int cmp(const void *a, const void *b) {
  const Enemy *enemy1 = (const Enemy *)a;
  const Enemy *enemy2 = (const Enemy *)b;
  
  if(enemy1->x > enemy2->x) return 1;
  else if(enemy1->x < enemy2->x) return -1;
  else {
    if(enemy1->y > enemy2->y) return 1;
    if(enemy1->y < enemy2->y) return -1;
  }
  
  return 0;
}

int LoadEnemies(Enemy *enemies, unsigned *cap) {
  //
  int camX, camY, relX, relY;
  char LBracket, comma, RBracket, LBrace;
  
  unsigned enemyCnt = 0;
  
  printf("Snapshots:\n");
  
  int argc;
  while( (argc = scanf(" %c %d %c %d %c %c", &LBracket, &camX, &comma, &camY, &RBracket, &LBrace)) != EOF) {
    if(argc != 6) return -1;
    if(LBracket != '[' || comma != ',' || RBracket != ']' || LBrace != '{') return -1;
    
    char nextChar;
    while( (argc = scanf(" %c %d %c %d %c %c", &LBracket, &relX, &comma, &relY, &RBracket, &nextChar)) != EOF) {
      if(LBracket == '}') break; //end of input for this cam
      if(argc != 6) return -1;
      if(LBracket != '[' || comma != ',' || RBracket != ']' || (nextChar != ',' && nextChar != '}') ) return -1;
      
      enemies[enemyCnt].x = camX + relX;
      enemies[enemyCnt].y = camY + relY;
      enemyCnt++;
      
      if(enemyCnt == *cap) {
        *cap *= 2;
        enemies = (Enemy *)realloc(enemies, (*cap) * sizeof(Enemy) );
      }
      
      if(nextChar == '}') break;
    }
  }
  
  return enemyCnt;
}

unsigned FindDuplicates(Enemy *enemies, int enemyCnt) {
  //
  unsigned duplicates = 0;
  for(int i = 0; i < enemyCnt; i++) {
    int j = i + 1;
    while(enemies[i].x == enemies[j].x && enemies[i].y == enemies[j].y) {
      duplicates++;
      j++;
      if(j == enemyCnt) break;
    }
    i = j - 1;
  }
  
  return duplicates;
}

void PrintEnemies(Enemy *enemies, int enemyCnt) {
  //
  for(int i = 0; i < enemyCnt; i++) {
    printf("[%d, %d]\n", enemies[i].x, enemies[i].y);
  }
}

void CleanUp(Enemy *enemies) {
  //
  free(enemies);
}

int main() {
  
  unsigned cap = 100;
  int enemyCnt = 0;
  Enemy *enemies = Init(cap);
  
  enemyCnt = LoadEnemies(enemies, &cap);
  if(enemyCnt == -1) {
    printf("Invalid input.\n");
    free(enemies);
    return 1;
  }
  
  qsort(enemies, enemyCnt, sizeof(Enemy), cmp);
  
  PrintEnemies(enemies, enemyCnt);
  
  unsigned duplicateCnt = FindDuplicates(enemies, enemyCnt);
  
  printf("Enemies: %d\n", enemyCnt - duplicateCnt);

  free(enemies);
}