#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
    struct Item *nextItem;
    char val;
} Item;

Item* CreateItem(char value, Item *next) {
    Item *newItem = (Item*)malloc(1 * sizeof(Item));

    newItem->val = value;
    newItem->nextItem = next;

    return newItem;
}

void DeleteItems(Item *item) {
    while(item) {
        Item* nextItem = item->nextItem;
        free(item);
        item = nextItem;
    }
}

Item* GetSeed(Item *item) {
    //
    if(!item) return NULL;

    Item *root = (Item*)malloc(1 * sizeof(Item));
    root->nextItem = NULL;
    root->val = item->val;
    Item *rootLastItem = root;

    item = item->nextItem;

    while(item) {
        Item *tmpItem = item;
        Item *tmpRoot = root;
        int belongsToRoot = 0;

        while(tmpItem) {
            if(tmpRoot == NULL) tmpRoot = root;
            if(tmpItem->val != tmpRoot->val) {
                belongsToRoot = 1;
                break;
            }
            tmpRoot = tmpRoot->nextItem;
            tmpItem = tmpItem->nextItem;
        }

        if(!belongsToRoot) break;

        Item *newRootMember = (Item*)malloc(1 * sizeof(Item));
        newRootMember->nextItem = NULL;
        newRootMember->val = item->val;
        rootLastItem->nextItem = newRootMember;

        item = item->nextItem;
    }

    return root;
}

void PrintItems(Item *list) {
    //
    while(list) {
        printf("%c", list->val);
        list = list->nextItem;
        if(list) printf(" -> ");
    }
    printf("\n");
}

int main() {

    Item *list = NULL;

    list = CreateItem('a', list);
    list = CreateItem('b', list);
    list = CreateItem('c', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);
    list = CreateItem('b', list);
    list = CreateItem('c', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);

    PrintItems(list);

    Item *seed = GetSeed(list);
    PrintItems(seed);

    free(list);
    free(seed);

    list = CreateItem('a', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);
    list = CreateItem('a', list);
    
    seed = GetSeed(list);
    PrintItems(seed);

    free(list);
    free(seed);
}